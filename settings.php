//<?php

$form->add( new \IPS\Helpers\Form\Editor( 'globalMessage_content', \IPS\Settings::i()->globalMessage_content, FALSE, array( 'app' => 'core', 'key' => 'Admin', 'autoSaveKey' => 'globalMessage_content' ) ) );

if ( $values = $form->values() )
{
	$form->saveAsSettings();
	\IPS\Db::i()->update( 'core_members', array( 'globalMessage_dismissed' => 0 ) );
	return TRUE;
}

return $form;