//<?php

class hook90 extends _HOOK_CLASS_
{

public function dismissGlobalMessage()
    {
		\IPS\Session::i()->csrfCheck();
		
		if ( \IPS\Member::loggedIn()->member_id )
		{
			\IPS\Member::loggedIn()->globalMessage_dismissed = TRUE;
			\IPS\Member::loggedIn()->save();
		}
		else
		{
			\IPS\Request::i()->setCookie( 'globalMessage_dismissed', TRUE );
		}
		
		\IPS\Output::i()->redirect( isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : \IPS\Http\Url::internal( '' ) );
	}
  
}